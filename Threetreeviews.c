// Name: Tree three views
// Time: 09:07 04.25.2018
// Author: Azimjon Kamolov
// Contact: azimjon.6561@gmail.com
// Purpose:	different tree traversals
// This program consists of structure and functions

#include <stdio.h>				// HEADER
#include <stdlib.h>				// HEADER FOR STRUCTURE
 

struct node						// STRUCTURE
{
	
     int data; 					// DATA
     struct node* left;			// POINTER TO LEFT CHILD
     struct node* right;		// POINTER TO RIGHT CHILD
};
 

struct node* newNode(int data)
{
     struct node* node = (struct node*)malloc(sizeof(struct node));	// TO ALLOCATE A NEW NODE
     node->data = data; 											// TO GIVE NODE DATA
     node->left = NULL;												// TO MAKE POINTER LEFT NULL
     node->right = NULL;											// TO MAKE POINTER RIGHT NULL
 
     return(node);													// TO RETURN NODE
}
 
void Post(struct node* node) // POSTORDER FUNC TO PRINT BINARY TREE ACCORDING TO POSTORDER TRAVERSAL
{
 	if (node == NULL)	// TO CHECK IF NODE IS NOT EMPTY
        return; // IF SO, JUST RETURNS NOTHING

    Post(node->left);      // FIRST RECURN ON LEFT SUBTREE
    Post(node->right);	 // THEN RECURN ON RIGT SUBTREE
    printf("%d ", node->data);		// TO DEAL WITH NODE HERE
}
 
void In(struct node* node) // INORDER FUNC TO PRINT BINARY TREE ACCORDING TO INORDER TRAVERSAL
{
    if (node == NULL)	// TO CHECK IF NODE IS NOT EMPTY
        return; // IF SO, JUST RETURNS NOTHING
 
    In(node->left);      // FIRST RECURN ON LEFT SUBTREE
    printf("%d ", node->data);  	// TO DEAL WITH NODE HERE
    In(node->right);    // THEN RECURN ON RIGT SUBTREE
}
 
void Pre(struct node* node)
{
    if (node == NULL)	// TO CHECK IF NODE IS NOT EMPTY
        return; // IF SO, JUST RETURNS NOTHING
 
    printf("%d ", node->data);          // TO DEAL WITH NODE HERE
    Pre(node->left);         // FIRST RECURN ON LEFT SUBTREE
    Pre(node->right);	   	// THEN RECURN ON RIGT SUBTREE
}    
 
int main() 	// MAIN FUNCTION
{
     struct node *root  = newNode(1);
     root->left = newNode(2);
     root->right = newNode(3);
     root->left->left = newNode(4);
     root->left->right = newNode(5); 
 
     printf("Preorder traversal of binary tree is \n"); // TO GIVE INFO ABOUT WHICH ONE IS OUT
     Pre(root); // TO CALL PREORDER FUNC
 
     printf("\nInorder traversal of binary tree is \n"); // TO GIVE INFO ABOUT WHICH ONE IS OUT
     In(root);  // TO CALL INORDER FUNC
 
     printf("\nPostorder traversal of binary tree is \n"); // TO GIVE INFO ABOUT WHICH ONE IS OUT
     Post(root); // TO CALL POSTORDER FUNC
 
     getchar();
     return 0;
}
